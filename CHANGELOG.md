# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.4](https://gitlab.com/yingleiming/commitizen-template/compare/v0.0.3...v0.0.4) (2024-05-29)


### Features

* 添加一行文本 ([54622df](https://gitlab.com/yingleiming/commitizen-template/commit/54622df3002afa4fe6960bfc3eb72c30c994e05f))

### 0.0.3 (2024-05-29)


### Features

* commitizen 基础配置 ([3e79d24](https://gitlab.com/yingleiming/commitizen-template/commit/3e79d24080ab4cc6e7c71f689fc252993831716c))

### 0.0.2 (2024-05-29)

### 0.0.1 (2024-05-29)
